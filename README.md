# canvas-test

# 2022-11-08

a recent test found, in a local package ( say, package A ) which requires canvas, when that package A is required by another package ( package B ), the installation time of package B varies greatly depending on the package manager used.

with npm, it takes around 3 seconds for initial installation, and less than 1 second for re-installation ( in the case of updating package A, for example );

with yarn, it takes more than 3 minutes for initial installation, and more than 1 minute for re-instasllations.

---

# 2022-11-08, update

a "member" answered the issue and proposed the use of either one of the 2 protocols `link:` or `portal:`, i.e. : 

instead of :
```
  "dependencies": {
    "test-canvas": "../package-A"
  }
```


do these :

```
  "dependencies": {
    "test-canvas": "portal:../package-A"
  }
```

but that "member" just closed the case without explaining why it only happens to [canvas](https://www.npmjs.com/package/canvas), which is rather too abrupt.


